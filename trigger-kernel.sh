#!/bin/bash
# Copyright 2024-present Linaro Limited
#
# SPDX-License-Identifier: MIT

KERNEL_URL=${KERNEL_URL:-"https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git"}

LAVA_TEST_PLANS_PROJECT=${LAVA_TEST_PLANS_PROJECT:-"lkft"}
QA_TEAM=${QA_TEAM:-"lkft"}
LINUX_KERNEL=${LINUX_KERNEL:-"linux-stable-rc"}
TUXCONFIG=${TUXCONFIG:-"tuxconfig"}

echo "CI_JOB_ID triggered from: ${TRIGGERED_FROM}"
echo "Kernel tree: ${KERNEL_URL}"
echo "Kernel branch: ${KERNEL_BRANCH}"
echo "Kernel sha: ${KERNEL_SHA}"

qa_project="${LINUX_KERNEL}-${KERNEL_BRANCH}"
./squad-client-cup.sh ${qa_project} "${LINUX_KERNEL} ${KERNEL_BRANCH}"
tuxconfig="${TUXCONFIG}/${KERNEL_BRANCH}-plan.yml"
tuxsuite build --git-repo ${KERNEL_URL} --git-sha ${KERNEL_SHA} --target-arch arm64 --toolchain gcc-13 --kconfig tinyconfig --show-logs --json-out build-${KERNEL_BRANCH}.json config
git_describe=$(jq -r '.git_describe' build-${KERNEL_BRANCH}.json)
callback="--callback ${QA_SERVER}/api/fetchjob/${QA_TEAM}/${qa_project}/${git_describe}/env/tuxsuite.com";
tuxsuite plan --git-repo ${KERNEL_URL} --git-sha ${KERNEL_SHA} "${tuxconfig}" --json-out build-${KERNEL_BRANCH}-plan.json --no-wait ${callback} --lava-test-plans-project ${LAVA_TEST_PLANS_PROJECT}
